import React from 'react';
import Food from './layouts/Food';
import foods from '../foods';

function Home({setLikes, likes}) {
    return (
        <div className="container main">
            <div className="row my-5">
                {foods.map( (food, index) => {
                    return (
                        <div className="col-md-6 col-lg-4 p-1"  key={index}>
                            <Food food = {food} setLikes={setLikes} likes={likes}/>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Home;