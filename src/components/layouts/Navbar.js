import React from 'react';

const Navbar = ({likes}) => {
    return ( 
        <nav className="navbar navbar-dark bg-dark position-fixed w-100">
            <div className="container px-0">
                <span className="navbar-brand mb-0 h1">Foodstagram</span>
                <div className="like-holder text-light">
                    <span><i className="fas fa-heart"></i></span>
                    <span className="ml-2">{parseInt(likes)}</span>
                </div>
            </div>
        </nav>
     );
}
 
export default Navbar;