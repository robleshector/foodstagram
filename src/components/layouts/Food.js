import React, {useState} from 'react';

const Food = ({food, setLikes, likes}) => {
    const [isLiked, setIsLiked] = useState(false);

    const handleClick = () => {
        if(!isLiked) {
            setIsLiked(true);
            setLikes(likes + 1);
        } else {

            setIsLiked(false);
            setLikes(likes - 1);
        }
    }

    return ( 
        <div className="food" onClick={handleClick}>
            <img src={`./assets/images/${food.img}`} alt="" className="img-fluid food__img"/>
            <div className={`heart ${isLiked? "liked" : "unliked"}`}>
                <i className="fas fa-heart"></i>
            </div>
            <div className="food__text">
                <h5 className="food__name">{food.name}</h5>
            </div>
        </div>
     );
}
 
export default Food;