export default [
    {"name": "Seafood Marinara", "img" : "1.jpg"},
    {"name": "Spaghetti", "img" : "2.jpg"},
    {"name": "Garden Pizza", "img" : "3.jpg"},
    {"name": "Hawaiian Pizza", "img" : "4.jpg"},
    {"name": "Margherita Pizza", "img" : "5.jpg"},
    {"name": "Pepperoni Pizza", "img" : "6.jpg"},
    {"name": "Caesar Salad", "img" : "7.jpg"}
]