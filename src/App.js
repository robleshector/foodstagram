import React, {Fragment, useState} from 'react';
import './App.css';

import Home from './components/Home';
import Navbar from './components/layouts/Navbar';

function App() {

  const [likes, setLikes] = useState(0);

  return (
    <Fragment>
      <Navbar likes={likes}/>
      <Home setLikes={setLikes} likes={likes}/>
    </Fragment>
  );
}

export default App;
